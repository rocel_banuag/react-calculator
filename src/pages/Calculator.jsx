import React, { useState, useEffect } from 'react';
import { Container, Form, Button } from 'react-bootstrap';

export default function Calc() {
 const [number1, setNumber1] = useState(0);
 const [number2, setNumber2] = useState(0);
 const [total, setTotal] = useState();

 function calculateAdd(e) {
    setTotal(number1 + number2); 
  }

  function calculateSubtract(e) {
    setTotal(number1 - number2); 
  }

  function calculateMultiply(e) {
    setTotal(number1 * number2); 
  }

  function calculateDivide(e) {
    setTotal(number1 / number2); 
  }

  function calculateReset(e) {
    setNumber1('');
    setNumber2('');
    setTotal(0);
  }

 return(
    <Container>
          <h1>Calculator</h1>
          <h1>{total}</h1>
        <Form>
            <Form.Control type="number" 
            placeholder="0" 
            value={number1}
            onChange={e => setNumber1(+e.target.value)}
            />
            <Form.Control type="number" 
            placeholder="0" 
            value={number2}
            onChange={e => setNumber2(+e.target.value)}
            />
            <Button onClick={calculateAdd}>Add</Button>
            <Button onClick={calculateSubtract}>Subtract</Button>
            <Button onClick={calculateMultiply}>Multiply</Button>
            <Button onClick={calculateDivide}>Divide</Button>
            <Button onClick={calculateReset}>Reset</Button>
        </Form>
      </Container>
 );
}
